<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\CoinBalanceWithLatestInfoResource;
use App\Http\Resources\TradeResource;
use App\CoinBalance;
use App\Trade;

class PortfolioController extends Controller
{
	/*
	 * The actions of that controller can´t be accesed by non authenticated user.
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}
	
	/*
	 * Retrieves a coinBalance list owned by the logged user.
	 */
    public function index()
	{
		return CoinBalanceWithLatestInfoResource::collection(CoinBalance::all());
	} 
	
	/*
	 * Creates a user Trade.
	 * In the creating of a Trade is followed by a coinBalance update that is performed by an TradeObserver class.
	 */
	public function store(Request $request)
	{
		$trade = Trade::create(request()->all());
		
		if(!$trade->exists)
		{
			return response()->json($trade->getErrors(), 400); 
		}

		return (new TradeResource($trade))->response()->setStatusCode(200);
	}
	
}
