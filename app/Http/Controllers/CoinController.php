<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Coin;
use App\Http\Resources\CoinCollection;
use App\Http\Resources\CoinWithLatestInfoResource;
use App\Http\Resources\HistoricalCollection;
use App\Http\Resources\HistoricalBasicInfoResource;
use Carbon\Carbon;

class CoinController extends Controller
{
	/*
	 * Retrieves a paginated coin list, with max item of 25 per page.
	 */
    public function index()
	{
		$pageSize = 25;
		
		return new CoinCollection( Coin::paginate($pageSize) );
	} 
	
	/*
	 * Retrieves coin data with the latest historical of the given coin .
	 */
	public function show(Coin $coin)
	{
		return new CoinWithLatestInfoResource( $coin );
	}
	
	/*
	 * Retrieves historical of given coin in a specific formated way.
	 */
	public function historical(Coin $coin)
	{
		$startDate = request('start_date');
		$endDate = request('end_date');

		return new HistoricalCollection( 
								HistoricalBasicInfoResource::collection(
									$coin->historicalBetweenDates($startDate,$endDate) 
								) 
							);
	}
}
