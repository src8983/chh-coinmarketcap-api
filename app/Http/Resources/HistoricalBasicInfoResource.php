<?php
/*
 * Allows to format the data of a Historical.
 */
namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class HistoricalBasicInfoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
		return 
		[
			'data' => 
			[
				'price_usd' => (float)$this->price_usd,
				'snapshot_at' => $this->snapshot_at,
			
			]
		];
		
    }
}
