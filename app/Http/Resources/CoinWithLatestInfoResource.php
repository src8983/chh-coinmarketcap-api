<?php
/*
 * Allows to format the data of a Coin to retrieve.
 * In that case the data is completed with the latest data of the given coin.
 */
namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CoinWithLatestInfoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
		
		$latest = $this->latest();
		
		return 
		[
			'data' => 
			[
				'id' => $this->id,
				'name' => $this->name,
				'symbol' => $this->symbol,
				'logo' => $this->logo,
				
				'rank' => (int)$latest->rank,
				'price_usd' => (float)$latest->price_usd,
				'price_btc' => (float)$latest->price_btc,
				'24h_volume_usd' => (int)$latest->getVolumeUsd24h(),
				'market_cap_usd' => (int)$latest->market_cap_usd,
				'available_supply' => (int)$latest->available_supply,
				'total_supply' => (int)$latest->total_supply,
				'percent_change_1h' => (float)$latest->percent_change_1h,
				'percent_change_24h' => (float)$latest->percent_change_24h,
				'percent_change_7d' => (float)$latest->percent_change_7d,
				'created_at' => $latest->created_at->toDateTimeString(),
				'updated_at' => $latest->updated_at->toDateTimeString(),
			
			]
		];
		
    }
}
