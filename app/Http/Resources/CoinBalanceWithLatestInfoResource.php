<?php
/*
 * Allows to format the data of a CoinBalance to retrieve.
 * In that case the data is completed with the latest price_usd of the given coin 
 * that is inside of a CoinBalance.
 */
namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CoinBalanceWithLatestInfoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
		$latest = $this->latest();

		return 
		[
			'data' => 
			[
				'coin_id' => (int)$this->coin_id,
				'amount' => (float)$this->amount,
				'price_usd' => (($latest == null)? null : (float)$latest->price_usd),
			]
		];
		
    }
}
