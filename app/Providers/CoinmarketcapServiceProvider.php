<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class CoinmarketcapServiceProvider extends ServiceProvider 
{
    public function register()
    {
        $this->app->bind('Coinmarketcap', \App\Coinmarketcap::class);
    }
}