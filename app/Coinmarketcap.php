<?php
/*
 * That class provides an connection with the api of coinmarketcap.
 *
 * Exists two different types of connection. 
 * One with the coinmarketcap API and the other with the chart ajax frontend of Coinmarketcap.com.
 */
namespace App;

class Coinmarketcap
{
	 const COIN_LIST_URI = 'https://api.coinmarketcap.com/v2/listings/';
	 
	 const HISTORICAL_LIST_URI = 'https://graphs2.coinmarketcap.com/currencies/{$slug}/1504246140000/1535782140000/';
	 //Important I´ve hardcoded the timestamp dates "1504246140000/1535782140000"
	 //An improvement would be to provide that dates as a parameter

	 /*
	  * Allow to connect with Coinmarketcap API providing the full CoinList.
	  */ 
	public function getCoinList()
	{
		return $this->myCurl(self::COIN_LIST_URI);
	}
	
	/*
	  * Allow to connect with chart ajax frontend of Coinmarketcap.com providing the a historical of a coinmarketcap
	  * provided by the slug.
	  */
	public function getHistorical($slug)
	{
		return $this->myCurl(
					str_replace('{$slug}',$slug,self::HISTORICAL_LIST_URI)
					);
	}
	
	/*
	 * Connection CURL configuration.
	 */
	public function myCurl($url)
	{
		$ch = curl_init();
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch,CURLOPT_URL,$url);
		curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,10);
		$data = curl_exec($ch);
		curl_close($ch);

		return json_decode($data);
	}
}
