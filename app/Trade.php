<?php
/*
 * Model that represents the Trade of that application.
 */
namespace App;

use App\Ownable;

class Trade extends Ownable
{
	public $fillable = ['notes','traded_at','total_usd','price_usd','amount','coin_id','user_id'];
	
    protected static $rules = [
	 'user_id' => 'required|exists:users,id',
	 'coin_id' => 'required|exists:coins,id',
	 'amount' => 'required|numeric',
	 'price_usd' => 'required|numeric',
	 'total_usd' => 'required|numeric',
	 'traded_at' => 'required',
	 ];
}
