<?php
/*
 * Model that represents the Historical of a Coin in that application.
 */
namespace App;

use App\Elegant;

class Historical extends Elegant
{
	 protected static $rules = [
	 'coin_id' => 'required|exists:coins,id',
	 'rank' => 'nullable|integer',
	 'price_usd' => 'nullable|numeric|min:0',
	 'price_btc' => 'nullable|numeric|min:0',
	 '24h_volume_usd' => 'nullable|integer|min:0',
	 'market_cap_usd' =>'nullable|integer|min:0',
	 'available_supply' =>'nullable|integer|min:0',
	 'total_supply' => 'nullable|integer|min:0',
	 'percent_change_1h' => 'nullable|numeric',
	 'percent_change_24h' => 'nullable|numeric',
	 'percent_change_7d' => 'nullable|numeric',
	 'snapshot_at' => 'required'
	 ];
	 
	 /*
	  * Scope for get the latest historical with more $query sql conditions.
	  */
	public function scopeLatest($query)
	{
		$query>orderBy('snapshot_at','desc')->first();
	}
	
	/*
	 * That method exists just because in PHP you can´t access a field that starts with a number.
	 * So is a ugly fix. An improvement would be call the field "volume_usd_24" instead.
	 */
	public function getVolumeUsd24h()
	{
		$problematicFieldName = '24h_volume_usd';
		
		return $this->$problematicFieldName;
	}
	
	/*
	 * Scope for get the historical by coin and timestamp.
	 */
	public function scopeByCoinAndTimestamp($query,$coinID,$snapshotAt)
	{
		$query->where('coin_id','=',$coinID)->where('snapshot_at','=',$snapshotAt);
	}
}
