<?php
/*
    References:
	https://github.com/JeffreyWay/Laravel-Model-Validation/blob/master/src/Way/Database/Model.php
	https://stackoverflow.com/questions/32774629/laravel-5-model-validation-and-authorization?utm_medium=organic&utm_source=google_rich_qa&utm_campaign=google_rich_qa
	https://stackoverflow.com/questions/38685019/laravel-how-to-create-a-function-after-or-before-saveupdate?utm_medium=organic&utm_source=google_rich_qa&utm_campaign=google_rich_qa

	
	That class allows to validate the business rules inside the model.
	So the model just need to has inheritance from that class and
	fill the $rules array with his own rules.
*/
namespace App;

use Illuminate\Database\Eloquent\Model;
use Validator;

class Elegant extends Model
{
    protected static $rules = [];

	protected static $messages = [];
	
    protected $errors;
	
	/**
     * Allow to init the model with an action binded
	 * to a specific event "saving".
     */
	protected static function boot()
    {
        parent::boot();
		
        static::saving(function($model)
        {
            return $model->validate();
        });
    }
	
	 /**
     * Validates current attributes against rules
     */
    public function validate()
    {
        $v = Validator::make($this->attributes, static::$rules, static::$messages);
        if ($v->passes())
        {
            return true;
        }
        $this->setErrors($v->messages());
        return false;
    }
	
	/**
     * Set error message bag
     * 
     * @var Illuminate\Support\MessageBag
     */
    protected function setErrors($errors)
    {
        $this->errors = $errors;
    }
    /**
     * Retrieve error message bag
     */
    public function getErrors()
    {
        return $this->errors;
    }
    /**
     * Inverse of wasSaved
     */
    public function hasErrors()
    {
        return ! empty($this->errors);
    }
}
