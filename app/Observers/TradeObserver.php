<?php
 /*
  * That Observer has the unic responsability 
  * to listen if a trade is created and 
  * update the coinBalance in consecuence.
  */
namespace App\Observers;
 
use App\Trade;
use App\CoinBalance;
 
class TradeObserver
{
    public function created(Trade $trade)
    {
        //Update the CoinBalance by user
		
		$coinBalance = 
		CoinBalance::where('coin_id','=',$trade->coin_id)
		                 ->where('user_id','=',$trade->user_id)
						 ->first();
		
		if($coinBalance == null)
		{
			$coinBalance = new CoinBalance;
			$coinBalance->coin_id = $trade->coin_id;
			$coinBalance->user_id = $trade->user_id;
			$coinBalance->amount = 0;
		}
		
		$coinBalance->amount  = ($coinBalance->amount  +  $trade->amount);
		$coinBalance->save();
    }
}
