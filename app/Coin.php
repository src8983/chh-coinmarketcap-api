<?php
/*
 * Model that represents the Coin of that application.
 */
namespace App;

use Carbon\Carbon;
use App\Elegant;

class Coin extends Elegant 
{
	protected static $rules = [
	 'name' => 'required|unique:coins',
	 'symbol' => 'required|unique:coins',
	 ];
	 
	/*
	 * Method Relation with historical Model.
	 */ 
    public function historical()
    {
        return $this->hasMany('App\Historical');
    }
	
	/*
	 * Method that retuns the latest historical data related with a coin.
	 */ 
	public function latest()
    {
        return $this->hasMany('App\Historical')->orderBy('snapshot_at','desc')->first();
    }
	
	/*
	 * That method allows to find by dates inside the relation with "historical".
	 */
	public function historicalBetweenDates($startDate=null,$endDate=null)
	{
		$query = $this->historical;
		
		if($startDate != null)
			$query = $query->where('snapshot_at','>=',Carbon::parse($startDate));
		
		if($endDate != null)
			$query = $query->where('snapshot_at','<=',Carbon::parse($endDate));
		
		return $query;
	}
}
