<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * @method static mixed check()
*/
class Coinmarketcap extends Facade
{
    protected static function getFacadeAccessor() { return 'Coinmarketcap'; }
}