<?php
/*
 * Model that represents the CoinBalance of that application.
 * Each Trade of a coin performed by a user updates that model.
 * So that model is the total balance per coin that has a user. 
 */
namespace App;

use App\Ownable;

class CoinBalance extends Ownable
{
	protected static $rules = [
	 'coin_id' => 'required|exists:coins,id',
	 'user_id' => 'required|exists:users,id',
	 'amount' => 'required|numeric',
	 ];
	 
	/*
	 * Method that retuns the latest historical data related with a coin.
	 */ 
    public function latest()
    {
        return $this->hasMany('App\Historical','coin_id')->orderBy('snapshot_at','desc')->first();
    }
	
}
