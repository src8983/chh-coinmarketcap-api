<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Facades\Coinmarketcap;
use App\Coin;

class PopulateCoin extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'populate:coin';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Populate Coins from Coinmarketcap';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $response = Coinmarketcap::getCoinList();
		
		foreach($response->data as $row)
		{
			$name = $row->name;
			$symbol = $row->symbol;
			$logo = 'https://s2.coinmarketcap.com/static/img/coins/16x16/'.$row->id.'.png';

			$this->populateCoin($name,$symbol,$logo);
		}
    }
	
	private function populateCoin($name,$symbol,$logo)
	{
		$result = false;
		$coin = Coin::where('name','=',$name)->first();
			
		if($coin == null)
		{
			$coin = new Coin;
			$coin->name = $name;
			$coin->symbol = $symbol;
			$coin->logo = $logo;
			$result = $coin->save();
		}
		
		return $result;
	}
}
