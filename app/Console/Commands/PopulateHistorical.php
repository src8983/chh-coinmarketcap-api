<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Facades\Coinmarketcap;
use App\Coin;
use App\Historical;

class PopulateHistorical extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'populate:historical';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Populate Historical through Coinmarketcap Chart Ajax front end';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $response = Coinmarketcap::getCoinList();
		
		//Foreach Coin
		foreach($response->data as $row)
		{
			echo $row->name."\n";
			
			try
			{
				//Just if the coin exist on Mysql DB try ...
				
				$coinID = $this->getCoinId($row->symbol);
			
				$buffer = $this->historicalDataToBuffer($row->website_slug);
				
				//Populate DB
				foreach($buffer as $time => $data)
				{
					
					echo $time ."\n";

					$this->populateHistorical($coinID,$time,
														$data['price_btc'],
														$data['price_usd'],
														$data['volume_usd'],
														$data['market_cap_by_available_supply']) ;
				}
			}
			catch(\Exception $e)
			{
				;
			}
			
		}
    }
	
	/*
	 * That method allow to get the internat Mysql Coin Id vía mapping.
	 * If the coin is not in DB it throw an exception 
	 */
	private function getCoinId($symbol)
	{
		$coin = Coin::where('symbol','=',trim($symbol))->first();
		if($coin != null)
			return $coin->id;
		
		throw new \Exception('The coin '.$symbol.' doesn´t exist.');
	}
	
	/*
	 * That method get data from Historical, extract and formated to prepare a buffer
	 * for insert.
	 */
	private function historicalDataToBuffer($slug)
	{
		$historical = Coinmarketcap::getHistorical($slug);
		
		//dd($historical);
		$buffer = [];
		
		//Load the buffer with all the data indexed by the timestamp
		foreach($historical as $concept => $data)
		{
			//var_dump($concept);
			
			foreach($data as $row)
			{
				if(isset($row[0]) && isset($row[1]))
				{
					$time = date("Y-m-d H:i:s", ($row[0]/1000));
					// time : $row[0]; value of concept :  $row[1];
					$buffer[ $time ][$concept] = $row[1];
				}
			}
		}
		
		return $buffer ;
	}
	
	
	/*
	 * That method insert each line of buffer on the historical MysqlDB.
	 */
	private function populateHistorical($coinID,$time,
														$price_btc, $price_usd, $volume, $available_supply)
	{
		$result = false;
		$historical = Historical::byCoinAndTimestamp($coinID,$time)->first();
		
		if($historical == null)
		{
			$historical = new Historical;
			$historical->coin_id = $coinID;
			$historical->snapshot_at = $time;
			$historical->price_usd = $price_usd;
			$historical->price_btc = $price_btc;
			
			$problematicFieldName = '24h_volume_usd';
			$historical->$problematicFieldName = $volume;
			$historical->available_supply = $available_supply;
			$result = $historical->save();
			
			//dd($historical);
		}
		
		return $result;
	}
}
