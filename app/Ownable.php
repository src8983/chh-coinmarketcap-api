<?php

/*
	That class allows to manage the funcionality respect a model
	owned by a user.
	So the model need to has a "user_id" attribute.
	That attribute is filled with the current logged user data.
	Also allow to search that model by the current logged user 
	in a transparent way.
 */

namespace App;

use App\Elegant;

class Ownable extends Elegant
{
	public static function boot()
    {
		static::saving(function($model)
        {
            $model->user_id = auth()->id();
        });
		
		//For load Elegant validations
		parent::boot(); 
		
        static::addGlobalScope('user',function($query){
            $query->where('user_id',auth()->id());
        });
    }
}