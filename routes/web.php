<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/coins','CoinController@index');

Route::get('/coins/{coin}','CoinController@show'); 

Route::get('/coins/{coin}/historical','CoinController@historical'); 

Route::get('/portfolio','PortfolioController@index'); 

Route::post('/portfolio','PortfolioController@store'); 

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
