<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTradesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trades', function (Blueprint $table) {
            $table->increments('id');
			$table->unsignedInteger('user_id')->nullable(false);
			$table->foreign('user_id')->references('id')->on('users');
			$table->unsignedInteger('coin_id')->nullable(false);
			$table->foreign('coin_id')->references('id')->on('coins');
			$table->index(['coin_id', 'user_id']);
			$table->float('amount')->nullable(false);
			$table->float('price_usd')->nullable(false);
			$table->float('total_usd')->nullable(false);
			$table->string('notes')->nullable();
			$table->dateTime('traded_at')->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trades');
    }
}
