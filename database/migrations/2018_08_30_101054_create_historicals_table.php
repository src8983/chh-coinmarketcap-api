<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistoricalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('historicals', function (Blueprint $table) {
            $table->increments('id');
			$table->unsignedInteger('coin_id');
			$table->foreign('coin_id')->references('id')->on('coins');
			$table->integer('rank')->nullable();
			$table->float('price_usd')->nullable();
			$table->float('price_btc')->nullable();
			$table->bigInteger('24h_volume_usd')->nullable();
			$table->bigInteger('market_cap_usd')->nullable();
			$table->bigInteger('available_supply')->nullable();
			$table->bigInteger('total_supply')->nullable();
			$table->float('percent_change_1h')->nullable();
			$table->float('percent_change_24h')->nullable();
			$table->float('percent_change_7d')->nullable();
			$table->dateTime('snapshot_at')->index();
			$table->index(['coin_id', 'snapshot_at']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('historicals');
    }
}
