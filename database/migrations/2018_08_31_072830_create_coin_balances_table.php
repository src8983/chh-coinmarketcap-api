<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoinBalancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coin_balances', function (Blueprint $table) {
            $table->increments('id');
			$table->unsignedInteger('user_id')->nullable(false);
			$table->foreign('user_id')->references('id')->on('users');
			$table->unsignedInteger('coin_id')->nullable(false);
			$table->foreign('coin_id')->references('id')->on('coins');
			$table->unique(['coin_id', 'user_id']);
			$table->float('amount')->nullable(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coin_balances');
    }
}
