<?php

use Faker\Generator as Faker;

$factory->define(App\Trade::class, function (Faker $faker) {
    return [
        'user_id' => function () {	
			return factory(App\User::class)->create()->id;
		},
         'coin_id' => function () {	
			return factory(App\Coin::class)->create()->id;
		},
		'amount' => $faker->randomFloat(2, 1, 100 ),
		'price_usd' => $faker->randomFloat(2, 1, 100 ),
		'total_usd' => $faker->randomFloat(2, 1, 100 ), 
		'notes' => $faker->name(),
        'traded_at'=>  $faker->dateTimeBetween($startDate = '-1 day', $endDate = 'now')->format("Y-m-d H:i:s"),
    ];
});
