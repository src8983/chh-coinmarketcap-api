<?php

use Faker\Generator as Faker;

$factory->define(App\CoinBalance::class, function (Faker $faker) {
    return [
		 'user_id' => function () {	
			return factory(App\User::class)->create()->id;
		},
         'coin_id' => function () {	
			return factory(App\Coin::class)->create()->id;
		},
		'amount' => $faker->randomFloat(2, 1, 100 ),
		//'price_usd' => $faker->randomFloat(2, 1, 100 ),
    ];
});
