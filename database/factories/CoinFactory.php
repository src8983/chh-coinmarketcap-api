<?php

use Faker\Generator as Faker;

$factory->define(App\Coin::class, function (Faker $faker) {
    return [
        'name' => $faker->unique()->name,//word(1),
		'symbol' => $faker->unique()->name,//word(1),
		'logo' => $faker->imageUrl(300,300), 
    ];
});
