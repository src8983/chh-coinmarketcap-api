<?php

use Faker\Generator as Faker;

$factory->define(App\Historical::class, function (Faker $faker) {
    return [
        'coin_id' => function () {	
			return factory(App\Coin::class)->create()->id;
		},
	
		'rank' => $faker->numberBetween(1,5),
		'price_usd' => $faker->randomFloat(2, 1, 100 ),
		'price_btc' => $faker->randomFloat(2, 0.01, 10 ),
		'24h_volume_usd' => $faker->numberBetween(100000000,500000000),
		'market_cap_usd' => $faker->numberBetween(100000000,500000000),
		'available_supply' => $faker->numberBetween(100000000,500000000),
		'total_supply' => $faker->numberBetween(100000000,500000000),
		'percent_change_1h' =>  $faker->randomFloat(2, 0.01, 100 ),
		'percent_change_24h' =>  $faker->randomFloat(2, 0.01, 100 ),
		'percent_change_7d' => $faker->randomFloat(2, 0.01, 100 ),
		'snapshot_at' => $faker->dateTimeBetween($startDate = '-1 day', $endDate = 'now')->format("Y-m-d H:i:s")
    ];
});
