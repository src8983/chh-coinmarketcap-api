<?php

namespace Tests\Feature;

use Tests\AuthenticatedTestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UpdateUserPortfolioTest extends AuthenticatedTestCase
{
	use DatabaseMigrations;
	
	protected $coin = null;
    
	protected function setUp ()
    {
        parent::setUp();
		
		//Create a coin and put a historical
		$this->coin = factory('App\Coin',null)->create();
		$startDate0='2018-06-30 00:00';
		$h0= factory('App\Historical',null)->create(['coin_id' => $this->coin->id,'snapshot_at' => $startDate0]);
    }
	
	/**
     * @test
     */
    public function it_can_update_user_portfolio()
    {
		$trade = factory('App\Trade',null)->make(['coin_id' => $this->coin->id,'user_id'=>null]);
        
		$this->post('/portfolio',$trade->toArray())
				->assertStatus(200)
				->assertJsonFragment([
					'coin_id'	=> $trade->coin_id,
					'user_id' =>$this->user->id,
					'amount' => $trade->amount,
					'price_usd' =>$trade->price_usd,
					'total_usd' => $trade->total_usd,
					'notes' => $trade->notes ,
					'traded_at' => $trade->traded_at,
				]);
    }
	
    /**
     * @test
     */	
	public function it_can_update_user_portfolio_only_autenticated_users()
    {
		$trade = factory('App\Trade',null)->make(['coin_id' => $this->coin->id,'user_id'=>null]);
		
		$this->signOut()
				->withExceptionHandling()
				->post('/portfolio',$trade->toArray())
				->assertRedirect('/login');
	}
	
	/**
     * @test
     */
    public function it_can_update_user_portfolio_many_times_geting_balance_updated()
    {
		$t1 = factory('App\Trade',null)->make(['coin_id' => $this->coin->id,'user_id'=>null]);
		$this->post('/portfolio',$t1->toArray());
		
		$t2 = factory('App\Trade',null)->make(['coin_id' => $this->coin->id,'user_id'=>null]);
		$this->post('/portfolio',$t2->toArray());
		
		$t3 = factory('App\Trade',null)->make(['coin_id' => $this->coin->id,'user_id'=>null]);
		$this->post('/portfolio',$t3->toArray());
		
		$t4 = factory('App\Trade',null)->make(['coin_id' => $this->coin->id,'user_id'=>null]);
		$this->post('/portfolio',$t4->toArray());
				
		//Important
		//The final import could be rounded so
		$totalAmount = round($t1->amount + $t2->amount + $t3->amount + $t4->amount,2 );
		
		$this->get('/portfolio')
				->assertJsonFragment([
						'amount'    => $totalAmount,
				]);
    }
	
}
