<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Coin;

class SchemaCoinsTest extends TestCase
{
	use DatabaseMigrations;
	
	/**
     *
     * @test
     */
    public function it_can_create_coins_with_name_and_symbol()
    {	
		$model= new Coin;
		$model->name = 'test';
		$model->symbol = 'test';
		
		$this->assertTrue($model->save()); 
    }
	
	/**
     *
     * @test
     */
    public function it_can_not_create_coins_without_name()
    {	
		$model= new Coin;
		$model->name = '';
		$model->symbol = 'test';
		
		$this->assertFalse($model->save()); 
    }
	
	/**
     *
     * @test
     */
    public function it_can_not_create_coins_without_symbol()
    {	
		$model= new Coin;
		$model->name = 'test';
		$model->symbol = '';
		
		$this->assertFalse($model->save()); 
    }
	
    /**
     *
     * @test
     */
    public function it_can_not_create_duplicated_coins_by_name()
    {
		$name = 'bitcoin';
		$coin1 = factory('App\Coin',null)->create(['name' => $name]);

		$coin2 = factory('App\Coin',null)->create(['name' => $name]);

		$this->assertTrue($coin2->hasErrors());
    }
	
	/**
     *
     * @test
     */
    public function it_can_not_create_duplicated_coins_by_symbol()
    {
		$symbol = 'BTC';
		$coin1 = factory('App\Coin',null)->create(['symbol' => $symbol]);
		
		$coin2 = factory('App\Coin',null)->create(['symbol' => $symbol]);
		
		$this->assertTrue($coin2->hasErrors());
    }
}
