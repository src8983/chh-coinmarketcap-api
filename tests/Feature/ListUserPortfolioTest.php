<?php

namespace Tests\Feature;

use Tests\AuthenticatedTestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ListUserPortfolioTest extends AuthenticatedTestCase
{
	use DatabaseMigrations;
    /**
     * @test
     */
    public function it_can_list_user_portfolio()
    {
		$coin1 = factory('App\Coin',null)->create();
		$startDate0='2018-06-30 00:00';
		$h0= factory('App\Historical',null)->create(['coin_id' => $coin1->id,'snapshot_at' => $startDate0]);
		
		$coinBalance1 = $this->createWithAuthUser('App\CoinBalance',['coin_id'=>$coin1->id],null);
		
		$coin2 = factory('App\Coin',null)->create();
		$h1= factory('App\Historical',null)->create(['coin_id' => $coin2->id,'snapshot_at' => $startDate0]);
		$coinBalance2 = $this->createWithAuthUser('App\CoinBalance',['coin_id'=>$coin2->id],null);
		
		//dd($coinBalance2 );
        
		$this->get('/portfolio')
				->assertStatus(200)
				->assertJsonFragment([
					[
						'coin_id'	=> $coinBalance1->coin_id,
						'amount'    => $coinBalance1->amount,
						'price_usd' => $h0->price_usd
					]
				])
				->assertJsonFragment([
					[
						'coin_id'	=> $coinBalance2->coin_id,
						'amount'    => $coinBalance2->amount,
						'price_usd' => $h1->price_usd
					]
				]);
    }
	
	/**
     * @test
     */
	public function it_can_list_user_portfolio_only_for_authenticated_users()
    {
		$this->signOut()
				->withExceptionHandling()->get('/portfolio')
				->assertRedirect('/login');
	}
	
	/**
     * @test
     */
    public function it_can_not_list_other_user_portfolio()
    {
		$coinBalance1 = $this->createWithAuthUser('App\CoinBalance',null,null);
		$this->signOut();
		
		$otherUser = factory('App\User',null)->create();
        $this->signIn($otherUser);
		
		$this->get('/portfolio')
				->assertStatus(200)
				->assertJsonMissing([
					[
						'coin_id'	=> $coinBalance1->coin_id,
						'amount'    => $coinBalance1->amount,
						'price_usd' => $coinBalance1->price_usd
					]
				]);
    }
	
}
