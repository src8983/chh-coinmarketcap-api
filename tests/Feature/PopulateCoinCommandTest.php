<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PopulateCoinCommandTest extends TestCase
{
	use DatabaseMigrations;
    /**
     *
     * @test
     */
    public function it_can_seed_coins()
    {
		$testReturn = json_decode('{"data":[{"id": 1, "name": "Bitcoin", "symbol": "BTC", "website_slug": "bitcoin"}, {"id": 2,"name": "Litecoin", "symbol": "LTC", "website_slug": "litecoin"}],"metadata": {"timestamp": 1535779044,"num_cryptocurrencies": 1910,"error": null}}');
		
		\App\Facades\Coinmarketcap::shouldReceive('getCoinList')->andReturn($testReturn);
		
		$this->artisan('populate:coin'); 
		
		$bitcoin = \App\Coin::where('name','=','Bitcoin')->first();
		$this->assertTrue($bitcoin ->exists);
		
		$litecoin = \App\Coin::where('name','=','Litecoin')->first();
		$this->assertTrue($litecoin ->exists);
    }
}
