<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ViewCoinsTest extends TestCase
{
	use DatabaseMigrations;

	/**
     * @test
     */
    public function it_can_display_a_coin_with_latest_historical_info()
    {
		$coin = factory('App\Coin',null)->create();
		$startDate1='2018-07-30 00:00';
        $h1= factory('App\Historical',null)->create(['coin_id' => $coin->id,'snapshot_at' => $startDate1]);
		$startDate2='2018-08-30 00:00';
		$h2= factory('App\Historical',null)->create(['coin_id' => $coin->id,'snapshot_at' => $startDate2]);
		
		$this->get('/coins/' . $coin->id)
		        ->assertStatus(200)
				->assertJsonFragment([
					'name'	=> $coin->name,
					'symbol' =>$coin->symbol,
					'logo' => $coin->logo,
					'rank' => $h2->rank,
					'price_usd' => $h2->price_usd,
					'price_btc' => $h2->price_btc,
					'24h_volume_usd' => $h2->getVolumeUsd24h(),
					'market_cap_usd' => $h2->market_cap_usd,
					'available_supply' => $h2->available_supply,
					'total_supply' => $h2->total_supply,
					'percent_change_1h' => $h2->percent_change_1h,
					'percent_change_24h' => $h2->percent_change_24h,
					'percent_change_7d' => $h2->percent_change_7d,
					'created_at' => $h2->created_at->toDateTimeString(),
					'updated_at' => $h2->updated_at->toDateTimeString()
				]);
    }
	
}
