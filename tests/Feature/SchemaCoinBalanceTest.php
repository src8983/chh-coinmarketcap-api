<?php

namespace Tests\Feature;

use Tests\AuthenticatedTestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Coin;

class SchemaCoinBalanceTest extends AuthenticatedTestCase
{
	use DatabaseMigrations;
	
	/**
     *
     * @test
     */
    public function it_can_not_create_coinbalance_without_coin()
    {	
		$historical = factory('App\CoinBalance',null)->create(['coin_id' => null]);

		$this->assertTrue($historical->hasErrors());
		
		$historical = factory('App\CoinBalance',null)->create(['coin_id' => 1]);

		//dd($historical->getErrors());
		$this->assertTrue($historical->hasErrors());
    }
	
	/**
     *
     * @test
     */
    public function it_can_not_create_coinbalance_duplicated()
    {	
		$this-> withExceptionHandling ();
		
		$user = factory('App\User',null)->create();
		$coin = factory('App\Coin',null)->create();
		
		$coinBalance = factory('App\CoinBalance',null)->create(['user_id' => $user->id,'coin_id' => $coin->id]);

		try
		{
			$eMessage = null;
			$coinBalance = factory('App\CoinBalance',null)->create(['user_id' => $user->id,'coin_id' => $coin->id]);
		}
		catch(\PDOException $e)
		{
			$eMessage = $e->getMessage();
		}

		$this->assertContains('UNIQUE constraint failed: coin_balances.coin_id, coin_balances.user_id',$eMessage); 
    }
	
	/**
     *
     * @test
     */
    public function it_can_not_create_coinbalance_with_nonnumeric_amount()
    {
		$coinBalance = factory('App\CoinBalance',null)->create(['amount' => 'shit word']);
		$this->assertTrue($coinBalance->hasErrors());
	}
}
