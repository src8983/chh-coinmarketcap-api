<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Coin;

class SchemaTradeUserTest extends TestCase
{
	use DatabaseMigrations;
	
	/**
     *
     * @test
     */
    public function it_can_not_create_trade_without_user()
    {	
		$trade = factory('App\Trade',null)->create(['user_id' => null]);

		$this->assertTrue($trade->hasErrors());
		
		$trade = factory('App\Trade',null)->create(['user_id' => 11]);

		$this->assertTrue($trade->hasErrors());
    }
}
