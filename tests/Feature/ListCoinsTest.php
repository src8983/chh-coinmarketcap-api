<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ListCoinsTest extends TestCase
{
	use DatabaseMigrations;
    /**
     *
     * @test
     */
    public function it_can_list_coins()
    {
		$coin = factory('App\Coin',null)->create();
		
		//dd($this->get('/coins'));
		
		$this->get('/coins')
		        ->assertStatus(200)
				->assertJsonFragment([
					'name'	=> $coin->name,
					'symbol' =>$coin->symbol,
					'logo' => $coin->logo
				]);
    }
	
	/**
     *
     * @test
     */
	public function it_can_list_paginated_coins_with_pagesize_25()
    {
		$pageSize = 25;
		$itemsTotal = $pageSize * 3 + 24;
		$collection = factory('App\Coin',$itemsTotal)->create();
		
		//dd($coin);
		$pageOne = $this->get('/coins')
		       ->assertStatus(200)
		       ->baseResponse->getData()->data;
			   
		$this->assertEquals(25, count($pageOne));
		
		$pageFour = $this->get('/coins?page=4')
		       ->assertStatus(200)
		       ->baseResponse->getData()->data;
			   
		$this->assertEquals(24, count($pageFour));
		
		$pageFive = $this->get('/coins?page=5')
		       ->assertStatus(200)
		       ->baseResponse->getData()->data;
			   
		$this->assertEquals(0, count($pageFive));
    }
}
