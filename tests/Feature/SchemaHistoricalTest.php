<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class SchemaHistoricalTest extends TestCase
{
	use DatabaseMigrations;
    /**
     *
     * @test
     */
    public function it_can_not_create_historical_without_coin()
    {
		$historical = factory('App\Historical',null)->create(['coin_id' => null]);

		$this->assertTrue($historical->hasErrors());
		
		$historical = factory('App\Historical',null)->create(['coin_id' => 1]);

		//dd($historical->getErrors());
		$this->assertTrue($historical->hasErrors());
    }
	
	 /**
     *
     * @test
     */
    public function it_can_not_create_historical_with_noninteger_rank()
    {
		$historical = factory('App\Historical',null)->create(['rank' => 'shit word']);
		
		$this->assertTrue($historical->hasErrors());
		
		$historical = factory('App\Historical',null)->create(['rank' => null]);
		
		$this->assertFalse($historical->hasErrors());
    }
	
	 /**
     *
     * @test
     */
    public function it_can_not_create_historical_with_negative_price()
    {
		$historical = factory('App\Historical',null)->create(['price_usd' => 'shit word']);
		$this->assertTrue($historical->hasErrors());
		
		$historical = factory('App\Historical',null)->create(['price_usd' => -1]);
		$this->assertTrue($historical->hasErrors());
		
		$historical = factory('App\Historical',null)->create(['price_btc' => 'shit word']);
		$this->assertTrue($historical->hasErrors());
		
		$historical = factory('App\Historical',null)->create(['price_btc' => -1]);
		$this->assertTrue($historical->hasErrors());
    }
	
	/**
     *
     * @test
     */
    public function it_can_not_create_historical_with_negative_integer_fields()
    {
		$historical = factory('App\Historical',null)->create(['24h_volume_usd' => 'shit word']);
		$this->assertTrue($historical->hasErrors());
		
		$historical = factory('App\Historical',null)->create(['24h_volume_usd' => -1]);
		$this->assertTrue($historical->hasErrors());
		
		$historical = factory('App\Historical',null)->create(['market_cap_usd' => 'shit word']);
		$this->assertTrue($historical->hasErrors());
		
		$historical = factory('App\Historical',null)->create(['market_cap_usd' => -1]);
		$this->assertTrue($historical->hasErrors());
		
		$historical = factory('App\Historical',null)->create(['available_supply' => 'shit word']);
		$this->assertTrue($historical->hasErrors());
		
		$historical = factory('App\Historical',null)->create(['available_supply' => -1]);
		$this->assertTrue($historical->hasErrors());
		
		$historical = factory('App\Historical',null)->create(['total_supply' => 'shit word']);
		$this->assertTrue($historical->hasErrors());
		
		$historical = factory('App\Historical',null)->create(['total_supply' => -1]);
		$this->assertTrue($historical->hasErrors());
    }
	
	/**
     *
     * @test
     */
    public function it_can_not_create_historical_with_nonnumeric_percentage_fields()
    {
		$historical = factory('App\Historical',null)->create(['percent_change_1h' => 'shit word']);
		$this->assertTrue($historical->hasErrors());
		
		$historical = factory('App\Historical',null)->create(['percent_change_24h' => 'shit word']);
		$this->assertTrue($historical->hasErrors());
		
		$historical = factory('App\Historical',null)->create(['percent_change_7d' => 'shit word']);
		$this->assertTrue($historical->hasErrors());
	}
	
	/**
     *
     * @test
     */
    public function it_can_not_create_historical_without_snapshot_at()
    {
		$historical = factory('App\Historical',null)->create(['snapshot_at' => null]);
		$this->assertTrue($historical->hasErrors());
	}
}
