<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ListHistoricalTest extends TestCase
{
	use DatabaseMigrations;
    /**
     * @test
     */
    public function it_can_list_historical()
    {
		$coin = factory('App\Coin',null)->create();
        $historical = factory('App\Historical',null)->create(['coin_id' => $coin->id]);
		
		$this->get('/coins/' . $coin->id . '/historical')
				->assertStatus(200)
				->assertJsonFragment([
					'price_usd'	=> $historical->price_usd,
					'snapshot_at' =>$historical->snapshot_at,
				]); 
    }
	
	/**
     * @test
     */
    public function it_can_not_list_historical_of_other_coin()
    {
		$coin1 = factory('App\Coin',null)->create();
        $h1 = factory('App\Historical',null)->create(['coin_id' => $coin1->id]);
		
		$coin2 = factory('App\Coin',null)->create();
        $h2 = factory('App\Historical',null)->create(['coin_id' => $coin2->id]);
		
		$this->get('/coins/' . $coin1->id . '/historical')
				->assertJsonFragment([
					'price_usd'	=> $h1->price_usd,
					'snapshot_at' =>$h1->snapshot_at,
				])
				->assertJsonMissing([
					'price_usd'	=> $h2->price_usd,
					'snapshot_at' =>$h2->snapshot_at,
				])
				; 
				
		$this->get('/coins/' . $coin2->id . '/historical')
				->assertJsonFragment([
					'price_usd'	=> $h2->price_usd,
					'snapshot_at' =>$h2->snapshot_at,
				])
				->assertJsonMissing([
					'price_usd'	=> $h1->price_usd,
					'snapshot_at' =>$h1->snapshot_at,
				])
				; 
    }
	
	 /**
     * @test
     */
    public function it_can_list_between_dates_the_historical()
    {
		$startDate0='2018-07-29 00:00';
		
		$coin = factory('App\Coin',null)->create();
		$startDate1='2018-07-30 00:00';
        $h1= factory('App\Historical',null)->create(['coin_id' => $coin->id,'snapshot_at' => $startDate1]);
		$startDate2='2018-08-30 00:00';
		$h2= factory('App\Historical',null)->create(['coin_id' => $coin->id,'snapshot_at' => $startDate2]);
		
		//dd(
		$this->get('/coins/' . $coin->id . '/historical?start_date='.$startDate0)
				->assertStatus(200)
			//	->baseResponse->getData()->data);
				->assertJsonFragment([
					'price_usd'	=> $h1->price_usd,
					'snapshot_at' =>$h1->snapshot_at,
				])
				->assertJsonFragment([
					'price_usd'	=> $h2->price_usd,
					'snapshot_at' =>$h2->snapshot_at,
				]);
		
		//dd(		
		$this->get('/coins/' . $coin->id . '/historical?start_date='.$startDate1)
			//	->baseResponse->getData()->data);
				->assertJsonFragment([
					'price_usd'	=> $h2->price_usd,
					'snapshot_at' =>$h2->snapshot_at,
				])
				->assertJsonMissing([
					'price_usd'	=> $h1->price_usd,
					'snapshot_at' =>$h1->snapshot_at,
				]);
    }
}
