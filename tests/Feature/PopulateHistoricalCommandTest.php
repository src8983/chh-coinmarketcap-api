<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PopulateHistoricalCommandTest extends TestCase
{
	use DatabaseMigrations;
    /**
     *
     * @test
     */
    public function it_can_seed_historical()
    {
		$testReturn = json_decode('{"data":[{"id": 1, "name": "Bitcoin", "symbol": "BTC", "website_slug": "bitcoin"}],"metadata": {"timestamp": 1535779044,"num_cryptocurrencies": 1910,"error": null}}');
		
		\App\Facades\Coinmarketcap::shouldReceive('getCoinList')->andReturn($testReturn);
		
		$this->artisan('populate:coin'); 
		
		$testReturn = 
		json_decode(
		'{"market_cap_by_available_supply": [[1535701140000, 120644617446], [1535701440000, 120539962755], [1535701740000, 120603583152]], 
		"price_btc": [[1535701140000, 1.0], [1535701440000, 1.0], [1535701740000, 1.0]], 
		"price_usd": [[1535701140000, 6997.42], [1535701440000, 6991.35], [1535701740000, 6995.04]],
		"volume_usd": [[1535701140000, 4614150000], [1535701440000, 4613470000], [1535701740000, 4609230000]]}');
		\App\Facades\Coinmarketcap::shouldReceive('getHistorical')->andReturn($testReturn);
		
		$this->artisan('populate:historical'); 
		
		$coin = \App\Coin::all()->first();
		$historical = \App\Historical::where('snapshot_at','=','2018-08-31 07:39:00')
		                                       ->where('coin_id','=',$coin->id)
											    ->first();
												
		$this->assertEquals($historical->price_usd,6997.42);
		$this->assertEquals($historical->available_supply,120644617446);	
    }
	
	 /**
     *
     * @test
     */
    public function it_can_not_seed_historical_without_coin_exists()
    {
		$testReturn = json_decode('{"data":[{"id": 1, "name": "Bitcoin", "symbol": "BTC", "website_slug": "bitcoin"}],"metadata": {"timestamp": 1535779044,"num_cryptocurrencies": 1910,"error": null}}');
		
		\App\Facades\Coinmarketcap::shouldReceive('getCoinList')->andReturn($testReturn);
		
		$testReturn = 
		json_decode(
		'{"market_cap_by_available_supply": [[1535701140000, 120644617446], [1535701440000, 120539962755], [1535701740000, 120603583152]], 
		"price_btc": [[1535701140000, 1.0], [1535701440000, 1.0], [1535701740000, 1.0]], 
		"price_usd": [[1535701140000, 6997.42], [1535701440000, 6991.35], [1535701740000, 6995.04]],
		"volume_usd": [[1535701140000, 4614150000], [1535701440000, 4613470000], [1535701740000, 4609230000]]}');
		\App\Facades\Coinmarketcap::shouldReceive('getHistorical')->andReturn($testReturn);
		
		$this->artisan('populate:historical'); 
		
		$historical = \App\Historical::all()->first();
		//dd($historical);
												
		$this->assertEquals(null,$historical);
    }
}
