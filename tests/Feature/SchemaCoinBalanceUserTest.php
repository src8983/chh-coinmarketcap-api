<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Coin;

class SchemaCoinBalanceUserTest extends TestCase
{
	use DatabaseMigrations;
	
	
	/**
     *
     * @test
     */
    public function it_can_not_create_coinbalance_without_user()
    {	
		$historical = factory('App\CoinBalance',null)->create(['user_id' => null]);

		$this->assertTrue($historical->hasErrors());
		
		$historical = factory('App\CoinBalance',null)->create(['user_id' => 1]);

		$this->assertTrue($historical->hasErrors());
    }
}
