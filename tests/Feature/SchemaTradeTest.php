<?php

namespace Tests\Feature;

use Tests\AuthenticatedTestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Coin;

class SchemaTradeTest extends AuthenticatedTestCase
{
	use DatabaseMigrations;
	
	/**
     *
     * @test
     */
    public function it_can_not_create_trade_without_coin()
    {	
		$trade = factory('App\Trade',null)->create(['coin_id' => null]);

		$this->assertTrue($trade->hasErrors());
		
		$trade = factory('App\Trade',null)->create(['coin_id' => 1]);

		//dd($historical->getErrors());
		$this->assertTrue($trade->hasErrors());
    }
	
	/**
     *
     * @test
     */
    public function it_can_not_create_trade_with_nonnumeric_amount()
    {
		$trade = factory('App\Trade',null)->create(['amount' => 'shit word']);
		$this->assertTrue($trade->hasErrors()); 
		
		$trade = factory('App\Trade',null)->create(['amount' => null]);
		$this->assertTrue($trade->hasErrors());
	}
	
	/**
     *
     * @test
     */
    public function it_can_not_create_trade_with_nonnumeric_price()
    {
		$trade = factory('App\Trade',null)->create(['price_usd' => 'shit word']);
		$this->assertTrue($trade->hasErrors()); 
		
		$trade = factory('App\Trade',null)->create(['price_usd' => null]);
		$this->assertTrue($trade->hasErrors());
	}
	
	/**
     *
     * @test
     */
    public function it_can_not_create_trade_with_nonnumeric_total()
    {
		$trade = factory('App\Trade',null)->create(['total_usd' => 'shit word']);
		$this->assertTrue($trade->hasErrors()); 
		
		$trade = factory('App\Trade',null)->create(['total_usd' => null]);
		$this->assertTrue($trade->hasErrors());
	}
	
	/**
     *
     * @test
     */
    public function it_can_not_create_trade_without_traded_at()
    {
		$trade = factory('App\Trade',null)->create(['traded_at' => null]);
		$this->assertTrue($trade->hasErrors());
	}
}
