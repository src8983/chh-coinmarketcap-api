<?php

namespace Tests\Feature;


use Tests\AuthenticatedTestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ModelCoinBalanceTest extends AuthenticatedTestCase
{
	use DatabaseMigrations;
    /**
     *
     * @test
     */
    public function it_can_get_latest_historical_in_model_coinbalance()
    {
		$coin = factory('App\Coin',null)->create();
		$startDate0='2018-06-30 00:00';
		$h0= factory('App\Historical',null)->create(['coin_id' => $coin->id,'snapshot_at' => $startDate0]);
		$startDate1='2018-07-30 00:00';
		$h1= factory('App\Historical',null)->create(['coin_id' => $coin->id,'snapshot_at' => $startDate1]);
		$startDate2='2018-08-30 00:00'; 
		$h2= factory('App\Historical',null)->create(['coin_id' => $coin->id,'snapshot_at' => $startDate2]);
		
		$coinBalance = $this->createWithAuthUser('App\CoinBalance',['coin_id' => $coin->id],null); 
		
		//dd($coinBalance);
		
		$historical = $coinBalance->latest();
		
		$this->assertEquals($historical->snapshot_at,$h2->snapshot_at);
    }
}
