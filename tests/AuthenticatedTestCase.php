<?php
namespace Tests;
use Exception;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use App\Exceptions\Handler;
use Illuminate\Contracts\Debug\ExceptionHandler;

abstract class AuthenticatedTestCase extends BaseTestCase
{
    use CreatesApplication;
	
	protected $user;
	
    protected function setUp ()
    {
        parent::setUp();
		$this->user = factory('App\User',null)->create();
		
        $this->signIn($this->user)
				->disableExceptionHandling();
    }
    protected function disableExceptionHandling ()
    {
        $this->oldExceptionHandler = app()->make(ExceptionHandler::class);
        app()->instance(ExceptionHandler::class, new PassThroughHandler);
    }
    protected function withExceptionHandling ()
    {
        app()->instance(ExceptionHandler::class, $this->oldExceptionHandler);
        return $this;
    }
	
	protected function signIn($user)
	{
		$this->actingAs($user);
		return $this;
	}
	
	protected function signOut()
	{
		$this->post('/logout');
		return $this;
	}
	
	protected function createWithAuthUser($class, $overrides = [], $times = null)
	{
		$overrides = $overrides != null?  $overrides : [];
		
		return factory($class, $times)->create(array_merge(['user_id' => $this->user->id], $overrides ) );
	}
	
	protected function makeWithAuthUser($class, $overrides = [], $times = null)
	{
		$overrides = $overrides != null?  $overrides : [];
		
		return factory($class, $times)->make(array_merge(['user_id' => $this->user->id], $overrides ));
	}
}
class PassThroughHandler extends Handler
{
    public function __construct () {}
    public function report (Exception $e) {}
    public function render ($request, Exception $e)
    {
        throw $e;
    }
}