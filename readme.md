**Some considerations**:

-   Coinmarketcap API, don´t has an endpoint for get historical data,
    just let you to get current data. So I´ve get it the data from
    Coinmarketcap ajax used in his front end. I´ve developed a
    Coinmarketcap service instead of install some other library.
-   The historical seeder I did traditional way, insert one and one. For
    get 6 month of historical data for each coin would be too slow. So I
    suggest an improvement that would be do a LOAD historical with
    REPLACE flag. That would seed the historical more quickly.

**Explanation**: 

All the models used has Elegant.php inheritance.
Elegant allows to validate the model business rules on time before to do
a transaction. The models has the responsability of his own data. I used
two Controllers for join the diverses use cases of the 4 endpoints. 


 **CoinController**:

1.  **index**: Uses the Coin model for get the results and a Collection
    for get a paginated JSON with prev, next, total etc.
2.  **show**: Uses the Coin model for get the specified item and a
    resource to join with the latest historical data.
3.  **historical**: Uses the Coin model method historicalBetweenDates
    that allows to get the relation "historical" filtered by dates. Also
    the JSON is filtered by a resource "HistoricalBasicInfoResource".

**PortfolioController**:

1.  Uses middleware auth for avoid to use by non authenticated users.
2.  The portfolio of a user is composed by coinbalances of each trade
    that the user has performed to the api.
3.  **index**: Gets the all CoinBalances (model) from a
    user.CoinBalances is a model Ownable.php that is **like a trait** that
    allows to manage the functionality respect the current user logged.
4.  **store**: Creates a Trade with the input data. An TradeObserver.php
    class is binded on time of created a trade for mantain the
    CoinBalance updated.

